# Programmieraufgabe PG
import numpy as np

# create Array
arr = np.arange(10007)
print(arr)

# read commands from file
def read():
    file = open("c:/Users/Thomas/Desktop/commands.txt", "r")
    count = 0
    for line in file:
        count =count +1
        print(count)
        if line[0] == "c":
            num = line.split(" ")
            num = str(num[1])
            num = num.split("\n")
            cut(num[0])
        else:
            sline = line.split(" ")
            if sline[3] == "stack\n":
                dins()
            else:   
                num = line.split(" ")
                num = str(num[3])
                num = num.split("\n")
                dwin(num[0])    

# deal into new stack
def dins():
    global arr
    arr = arr[::-1]
    print("-- DINS")
    print(arr)
    print("\n")
    
# cut N
def cut(num: int):
    global arr
    print(arr)
    num = int(num)
    arr1 = arr[num:arr.size:1]
    arr2 = arr[0:num:1]
    arr = np.concatenate((arr1,arr2), axis=0) 
    print("-- CUT", num)
    print(arr)
    print("\n")


# deal with increment N
def dwin(num: int):
    global arr
    num = int(num)
    arr1 = np.full(10007,-1)
    jump = 0
    #arr1[0] = arr[0]
    for i in arr:
        #print(arr1)
        #print("i=", i)
        if arr1[jump] == -1:
            #print("true","|jump=",jump,"  |cur element=", arr1[jump],"  |cur i= ",(i), "  |cur original=",arr[(i+1) %  arr.size])
            arr1[jump] = i
            jump = (jump + num) % arr.size
        else:
            #print("else",jump, arr1[jump])
            arr3 = arr1[jump:arr.size:1]
            next = np.where(arr3 == -1)
            #print("next", next[0][0])
            if next[0].size != 0:
                jump= next[0][0]
                arr1[jump] = i
                jump = (jump + num) % arr.size   
    arr = arr1
    print("-- DWIN", num)
    print(arr)
    print("\n")

# call to start the prpogram
read()

#cut(-2)
#cut(2)
#dwin(10)
print(arr)
result = np.where(arr == 2019)
 
print('Tuple of arrays returned : ', result)
print("Elements with value 2019 exists at following indices", result[0], sep='\n')
print(result)